// mapped_file.h
// VERSION 1.0 RC1
// DATE: 18.03.2016

#ifndef __MAPPED_FILE__
#define __MAPPED_FILE__

#if _WIN32 || _WIN64
	#include <BaseTsd.h>
	typedef UINT64 uint64_t;
#else
	#include <stdint.h>
#endif

#include <stdbool.h>

typedef enum mf_error_codes {
	MF_READ_ONLY = -10,
	MF_FILE_BUSY,
	MF_NO_MEMORY,
	MF_INVALID_VALUE,
	MF_OPEN_FAILED,
	MF_INTERNAL_ERROR,
	MF_SUCCESS = 0
} mf_error_t;

/*
For all functions listed below:
	All errors except MF_OPEN_FAILED, MF_FILE_BUSY
	and MF_READ_ONLY result in undefine behaviour in the future
	Any function might return MF_INTERNAL_ERROR or MF_NO_MEMORY
	All sizes and offsets are in bytes
	On unload, data must be flushed
	Standards should be backwards-compatible
*/

struct mf_handle;
typedef struct mf_handle *mf_handle_t;

/*
	Opens file and initializes mapped file structure, giving a handle to it
	
	name		- mapped file name
	pool_size	- maximum number of entries in the pool
	chunk_size	- chunk size (if 0, it is chosen by the realiztion)
	read_only	- if not 0, nothing is written to the file (even if mapped memory contents are changed)
	mf		- variable to store the handle
	
	returns	MF_SUCCESS		on success
		MF_OPEN_FAILED		if opening file failed
		MF_INVALID_VALUE	if pool_size == 0
		MF_INVALID_VALUE	if name or mf == NULL
			
	On error, contents of mf are undefined
*/
mf_error_t mf_open(const char* name, unsigned pool_size, uint64_t chunk_size, bool read_only, mf_handle_t* mf);

/*
	Closes the mapped file and frees all the associated data
	
	mf			- handle to the mapped file
	
	returns	MF_SUCCESS		on success
		MF_INVALID_VALUE	if mf == NULL
		MF_FILE_BUSY		if some chunks have non-null reference counters
*/
mf_error_t mf_close(mf_handle_t mf);

/*
	Flushes a all mapped data to file
	
	mf			- handle to the mapped file
	
	returns	MF_SUCCESS		on success
		MF_INVALID_VALUE	if mf == NULL
*/
mf_error_t mf_file_flush(mf_handle_t mf);

/*
	Writes file size to *dst
	
	mf			- handle to the mapped file
	dst			- pointer to store file size
	
	returns	MF_SUCCESS		on success
		MF_INVALID_VALUE	if mf == NULL of dest == NULL
*/
mf_error_t mf_file_size(mf_handle_t mf, uint64_t* dst);

/*
	Copies data from mapped file to memory region
	
	mf		- handle to the mapped file
	dst		- pointer to destination data
	offset_src	- the offset in the file of the source region
	size		- size of the copied data
	
	returns	MF_SUCCESS		on success
		MF_INVALID_VALUE	if mf == NULL
		MF_INVALID_VALUE	if offset_src region exceeds the file bounds
			
	If mapped source and destination regions overlap, the behaviour is undefined
*/
mf_error_t mf_read(mf_handle_t mf, void* dst, uint64_t offset_src, uint64_t size);

/*
	Copies data from memory region to mapped file
	
	mf		- handle to the mapped file
	off_dst		- the offset in the file of the destination region
	src		- pointer to source data
	size		- size of the copied data
	flush		- flush data to disk for modified mapped chunks
	
	returns	MF_SUCCESS		on success
		MF_INVALID_VALUE	if mf == NULL
		MF_INVALID_VALUE	if off_dst region exceeds the file bounds
		MF_READ_ONLY		if file was opened as read-only
			
	If source and mapped destination regions overlap, the behaviour is undefined
*/
mf_error_t mf_write(mf_handle_t mf, uint64_t off_dst, const void* src, uint64_t size);

/*
	Returns a string that describes the error code passed in the argument errcode

	errcode		- the error code, returned by some function from this library
	
	returns	        string: description of the error
*/
char *mf_strerror(mf_error_t errcode);

#endif // __MAPPED_FILE__
